<?php

namespace app\controllers;

use app\models\PostSearch;
use Yii;
use yii\web\Controller;
use app\models\Post;
use yii\web\NotFoundHttpException;
use app\models\PostFrontendSearch;

class PostController extends Controller
{


    public function actionIndex()
    {
        $searchModel = new PostFrontendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
            ]);
    }
    public function actionView($id)
    {
        $post = Post::PostById($id);
        if ($post) {
            return $this->render('view', ['model' => $post]);
        }
        throw new NotFoundHttpException('Пост не найден!');
    }
}