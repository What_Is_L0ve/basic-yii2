<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="row">
    <h2>
        <?= Html::a($model['title'], Url::toRoute(['post/view', 'id' => $model['id']])); ?>
    </h2>
    <p>
        <?= $model['text']; ?>
    </p>
</div>
